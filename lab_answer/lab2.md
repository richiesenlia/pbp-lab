1. Apakah perbedaan antara JSON dan XML?
Beberapa perbedaan antara JSON dan XML
1. objek JSON memiliki tipe sedangkan XML tidak memiliki tipe
2. jSON hanya mendukung UTF-8 encoding sedangkan XML mendukung beragam encoding
3. JSON tidak mendukung comment sedangkan XML mendukung comment
4. XML lebih aman dibanding json
5. Json tidak memiliki kemampuan untuk mendisplay data
6. json lebih mudah untuk di parse dibanding XML


7. Apakah perbedaan antara HTML dan XML?

Perbedaan antara HTML dan XML terdapat pada fungsi mereka. HTML didesain untuk menampilkan data sedangkan XML didesain untuk menyimpan dan mentransfer data.
Beberapa perbedaan teknis mengenai HTML dan XML
1. HTML menggunakan tag yang telah di *predefined* sedangkan XML tag didefined sesuai kebutuhan
2. HTML tidak case sensitive sedangkan XML case sensitive
3. HTML tidak menyimpan whitespaces sedangkan XML dapat menyimpan whitespaces